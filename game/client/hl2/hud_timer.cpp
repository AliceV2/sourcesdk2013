//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//
//
// implementation of CHudCountdown class
//
#include "cbase.h"
#include "hud.h"
#include "hud_macros.h"
#include "view.h"

#include "iclientmode.h"

#include <KeyValues.h>
#include <vgui/ISurface.h>
#include <vgui/ISystem.h>
#include <vgui_controls/AnimationController.h>

#include <vgui/ILocalize.h>

using namespace vgui;

#include "hl2mp_gamerules.h"

#include "hudelement.h"
#include "hud_basetimer.h"

#include "convar.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

//-----------------------------------------------------------------------------
// Purpose: Health panel
//-----------------------------------------------------------------------------
class CHudCountdown : public CHudElement, public CHudBaseTimer
{
	DECLARE_CLASS_SIMPLE( CHudCountdown, CHudBaseTimer );

public:
	CHudCountdown( const char *pElementName );
	virtual void OnThink();
	virtual void VidInit(void);
	virtual void Init(void);
	virtual void LevelInit(void);
	virtual void Reset();

private:
	// old variables
	float m_fTimerStart;
	float m_fTimerStartP;
	int m_iTimerRemain = -1;
	int m_iTimerRemainMS;
	int m_iTimerMSCount;
	char *m_szTimerLabel;

	bool notStopped = true;
	bool notExpired;

	Color FgColor;
};	

DECLARE_HUDELEMENT( CHudCountdown );

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CHudCountdown::CHudCountdown(const char *pElementName) : CHudElement(pElementName), CHudBaseTimer(NULL, "HudCountdown")
{
	SetProportional(true);
	SetPaintEnabled(false);
	SetPaintBackgroundEnabled(false);
	SetAlpha(0);
}

void CHudCountdown::Init()
{
	FgColor[0] = 0;
	FgColor[1] = 128;
	FgColor[2] = 255;
	FgColor[3] = 255;
	SetFgColor(FgColor);
}

void CHudCountdown::VidInit()
{
	int wx, wy, ww, wt;
	surface()->GetWorkspaceBounds(wx, wy, ww, wt); //(ww - GetWide()) / 2
	SetPos((ww - GetWide()) / 2, 4);
}

void CHudCountdown::LevelInit()
{
	m_iTimerRemain = -1;
	SetProportional(true);
	SetPaintEnabled(false);
	SetPaintBackgroundEnabled(false);
	SetFgColor(FgColor);
	SetAlpha(0);
}

void CHudCountdown::Reset()
{
	// aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
}

//-----------------------------------------------------------------------------
// Purpose: Think think think
//-----------------------------------------------------------------------------
void CHudCountdown::OnThink()
{
	CHL2MPRules *pRules = HL2MPRules();
	if (!pRules)
		return;
	// bark
	int iRemain = pRules->GetCountdownTimerRemain();
	int iRemainMS = pRules->GetCountdownMS();
	bool isPaused = pRules->m_bIsTimerPaused;

	//is timer paused?
	if (pRules->m_fTimerStartP != -1)
	{
		pRules->m_bIsTimerPaused = true;
		isPaused = true;
		return;
	}

	//update start time on resume
	if (isPaused && pRules->m_fTimerStartP == -1)
	{
		m_fTimerStart = pRules->m_fTimerStart;
		pRules->m_bIsTimerPaused = false;
		return;
	}

	// there was no timer and still is no timer or there's no new time to display
	if ((iRemain<0 && m_iTimerRemain<0) || (iRemain == m_iTimerRemain) && (iRemainMS == m_iTimerRemainMS))
		return;

	if (iRemain >= 0 && !(iRemainMS == 0 && iRemain == 0))
		SetMS(iRemainMS);

	m_iTimerRemainMS = iRemainMS;

	if (iRemain == m_iTimerRemain)
		return;

	//is stop input fired?
	if (pRules->m_fTimerStartP == -1 && pRules->m_fTimerStart == -1 && !notStopped)
	{
		g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("RoundtimerStop");
		notStopped = true;
		notExpired = true;
		m_iTimerRemain = -1;
		return;
	}

	// if we're here, there's was a timer before, but if's it's not there anymore, we need to hide it
	if (iRemain<0 || m_fTimerStartP>0 && !notExpired) {
		g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("RoundtimerExpired");
		SetSeconds(0);
		SetMS(0);
		notStopped = true;
		SetPaintEnabled(false);
		SetPaintBackgroundEnabled(false);
		return;
	}

	if (iRemain < 0 || m_fTimerStartP>0)
		return;

	// there was no timer before or the timer has been restarted
	if ((m_fTimerStart != pRules->m_fTimerStart) || m_iTimerRemain<0) {
		// start the init-event to reset the changing properties
		SetPaintBackgroundEnabled(true);
		SetPaintEnabled(true);
		notExpired = false;
		SetFgColor(FgColor);
		g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("RoundtimerInit");
		SetFgColor(FgColor);
		// save starttime to detect a timer-restart
		m_fTimerStart = pRules->m_fTimerStart;
	}

	// when the timer reaches 20, change it's color to red
	if (iRemain == 20){
		g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("RoundtimerBelow20");
	}
	// for every time below 10 make it pulse
	else if (iRemain<10)
		g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("RoundtimerPulse");
	// move it down for the last 5 seconds 
	else if (iRemain == 5)
		g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("RoundtimerBelow5");

	m_iTimerRemain = iRemain;
	m_szTimerLabel = pRules->m_szTimerNiggs;
	m_fTimerStartP = pRules->m_fTimerStartP;
	SetSeconds(m_iTimerRemain);
	SetLabelText(m_szTimerLabel);
	notStopped = false;
	//Msg("Lable2:%s\n", m_szTimerLabel);
}
