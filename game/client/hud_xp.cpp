//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//
//
// xp.cpp
//
// implementation of CHudXP class
//
#include "cbase.h"
#include "hud.h"
#include "hudelement.h"
#include "hud_macros.h"
#include "hud_numericdisplay.h"
#include "iclientmode.h"
#include "c_hl2mp_player.h"

#include "vgui_controls/AnimationController.h"
#include "vgui/ILocalize.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

#define INIT_BAT	-1

//-----------------------------------------------------------------------------
// Purpose: Displays suit power (armor) on hud
//-----------------------------------------------------------------------------
class CHudXP : public CHudNumericDisplay, public CHudElement
{
	DECLARE_CLASS_SIMPLE(CHudXP, CHudNumericDisplay);

public:
	CHudXP(const char *pElementName);
	void Init(void);
	void Reset(void);
	void VidInit(void);
	void OnThink(void);

};

DECLARE_HUDELEMENT(CHudXP);

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CHudXP::CHudXP(const char *pElementName) : BaseClass(NULL, "HudXP"), CHudElement(pElementName)
{
#ifdef SecobMod__HAS_BATTERY_REGARDLESS_OF_SUIT
	SetHiddenBits(HIDEHUD_PLAYERDEAD);
#else
	SetHiddenBits(HIDEHUD_HEALTH | HIDEHUD_NEEDSUIT);
#endif
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudXP::Init(void)
{
	Reset();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudXP::Reset(void)
{
	wchar_t *tempString = g_pVGuiLocalize->Find("#Hud_Experience");

	if (tempString)
	{
		SetLabelText(tempString);
	}
	else
	{
		SetLabelText(L"XP");
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudXP::VidInit(void)
{
	Reset();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudXP::OnThink(void)
{
	C_HL2MP_Player *pPlayer = C_HL2MP_Player::GetLocalHL2MPPlayer();
	if (pPlayer) {
		SetDisplayValue(pPlayer->GetXP());
	}
}
