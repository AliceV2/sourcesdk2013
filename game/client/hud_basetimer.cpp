//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: Draws a timer in the format "Minutes:Seconds"
// Seconds are padded with zeros
//
//=============================================================================//

#include "cbase.h"
#include "hudelement.h"
#include <vgui_controls/Panel.h>
#include <vgui/ISurface.h>
#include "hud_basetimer.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

using namespace vgui;

CHudBaseTimer::CHudBaseTimer(vgui::Panel *parent, const char *name) : BaseClass(parent, name)
{
	m_iSeconds = 0;
	m_iMSeconds = 0;
	SetProportional(true);
	SetPaintEnabled(false);
	SetPaintBackgroundEnabled(false);
}

void CHudBaseTimer::SetSeconds(int seconds)
{	
	m_iSeconds = seconds;
}

void CHudBaseTimer::SetMS(int ms)
{
	m_iMSeconds = ms;
}

void CHudBaseTimer::PaintTime(HFont font, int xpos, int ypos, int secs, int ms)
{
	SetVisible(false);
	surface()->DrawSetTextFont(font);
	wchar_t unicode[10];
	V_snwprintf(unicode, ARRAYSIZE(unicode), L"%.2d:%.2d:%.2d", secs / 60, secs % 60, ms);
	xpos = scheme()->GetProportionalScaledValue(50); //100

	surface()->DrawSetTextPos(xpos, ypos);
	surface()->DrawUnicodeString( unicode );
	SetVisible(true);
}

//-----------------------------------------------------------------------------
// Purpose: Set label
//-----------------------------------------------------------------------------
void CHudBaseTimer::PaintTimerLabel(HFont font)
{
	surface()->DrawSetTextFont(font);
	wchar_t unicode[256];
	V_strtowcs(m_szLabelText, -1, unicode, ARRAYSIZE(unicode));

	int width = 0;
	for (wchar_t *ch = unicode; *ch != 0; ch++)
	{
		width += surface()->GetCharacterWidth(font, *ch);
	}

	int nLabelHeight, nLabelWidth;
	surface()->GetTextSize(m_hTextFont, unicode, nLabelWidth, nLabelHeight);
	int xpos = text_xpos;
	int ypos = text_ypos;
	xpos = (GetWide() / 2 - nLabelWidth / 2);

	surface()->DrawSetTextColor(GetFgColor());
	surface()->DrawSetTextPos(xpos, ypos);
	surface()->DrawUnicodeString(unicode);
}

//-----------------------------------------------------------------------------
// Purpose: data accessor
//-----------------------------------------------------------------------------
void CHudBaseTimer::SetLabelText(char *text )
{
	m_szLabelText = text;
	//Msg("Label: %s\n", m_szLabelText);
}

void CHudBaseTimer::Paint()
{
	surface()->DrawSetTextColor(GetFgColor());
	PaintTime( m_hNumberFont, digit_xpos, digit_ypos, m_iSeconds, m_iMSeconds );

	// draw the overbright blur
	for (float fl = m_flBlur; fl > 0.0f; fl -= 1.0f)
	{
		if (fl >= 1.0f)
		{
			PaintTime(m_hNumberGlowFont, digit_xpos, digit_ypos, m_iSeconds, m_iMSeconds);
		}
		else
		{
			// draw a percentage of the last one
			Color col = GetFgColor();
			col[3] *= fl;
			surface()->DrawSetTextColor(col);
			PaintTime(m_hNumberGlowFont, digit_xpos, digit_ypos, m_iSeconds, m_iMSeconds);
		}
	}

	PaintTimerLabel(m_hTextFont);
}

void CHudBaseTimer::SetToPrimaryColor()
{
	SetFgColor(m_TextColor);
}

void CHudBaseTimer::SetToSecondaryColor()
{
	SetFgColor(m_FlashColor);
}
