//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#ifndef HUD_BASETIMER_H
#define HUD_BASETIMER_H
#ifdef _WIN32
#pragma once
#endif

#include "hud_numericdisplay.h"

//-----------------------------------------------------------------------------
// Purpose: Base class for all the hud elements that are just a numeric display
//			with some options for text and icons
//-----------------------------------------------------------------------------
class CHudBaseTimer : public CHudNumericDisplay
{
	DECLARE_CLASS_SIMPLE( CHudBaseTimer, CHudNumericDisplay );

public:
	CHudBaseTimer(vgui::Panel *parent, const char *name);

	void SetSeconds(int seconds);
	void SetMS(int ms);

protected:
	// vgui overrides
	virtual void Paint();
	void PaintTimerLabel(vgui::HFont font);

	void SetToPrimaryColor();
	void SetToSecondaryColor();
	virtual void SetLabelText(char* text);

private:
	void PaintTime(vgui::HFont font, int xpos, int ypos, int secs, int ms = 0);

	int m_iSeconds;
	int m_iMSeconds;
	char *m_szLabelText;

	CPanelAnimationVar( float, m_flBlur, "Blur", "0" );
	//CPanelAnimationVar( float, m_flAlphaOverride, "Alpha", "255" );
	CPanelAnimationVar( Color, m_TextColor, "TextColor", "FgColor" );
	CPanelAnimationVar( Color, m_FlashColor, "SecondaryColor", "FgColor" );

	CPanelAnimationVar( vgui::HFont, m_hNumberFont, "NumberFont", "HudNumbers" );
	CPanelAnimationVar( vgui::HFont, m_hNumberGlowFont, "NumberGlowFont", "HudNumbersGlow" );
	CPanelAnimationVar( vgui::HFont, m_hSmallNumberFont, "SmallNumberFont", "HudNumbersSmall" );
	CPanelAnimationVar( vgui::HFont, m_hTextFont, "TextFont", "HudCountdownText" );

	CPanelAnimationVarAliasType( float, text_xpos, "text_xpos", "32", "proportional_float" );
	CPanelAnimationVarAliasType( float, text_ypos, "text_ypos", "64", "proportional_float" );
	CPanelAnimationVarAliasType( float, digit_xpos, "digit_xpos", "32", "proportional_float" );
	CPanelAnimationVarAliasType( float, digit_ypos, "digit_ypos", "16", "proportional_float" );
	CPanelAnimationVarAliasType( float, digit2_xpos, "digit2_xpos", "0", "proportional_float" );
	CPanelAnimationVarAliasType( float, digit2_ypos, "digit2_ypos", "0", "proportional_float" );
};


#endif // HUD_BASETIMER_H
