//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: Spawns antlions
//
// $NoKeywords: $
//=============================================================================//

#include "cbase.h"
#include "basehlcombatweapon.h"
#include "engine/IEngineSound.h"
#include "npcevent.h"
#include "in_buttons.h"
#include "antlion_maker.h"
#include "gamestats.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

//
// Bug Spawner Weapon
//

class CWeaponBugSpawner : public CBaseHLCombatWeapon
{
	DECLARE_CLASS(CWeaponBugSpawner, CBaseHLCombatWeapon);
public:

	DECLARE_SERVERCLASS();

	CWeaponBugSpawner(void);

	void	Spawn(void);
	void	FallInit(void);

	int		CapabilitiesGet(void) { return bits_CAP_WEAPON_RANGE_ATTACK1; }

	void	Operator_HandleAnimEvent(animevent_t *pEvent, CBaseCombatCharacter *pOperator);

	void	Drop(const Vector &vecVelocity);
	void	BugbaitStickyTouch(CBaseEntity *pOther);
	void	OnPickedUp(CBaseCombatCharacter *pNewOwner);
	bool	Deploy(void);
	bool	Holster(CBaseCombatWeapon *pSwitchingTo);

	void	ItemPostFrame(void);
	void	Precache(void);
	void	PrimaryAttack(void);
	void	SecondaryAttack(void);
	void	ThrowGrenade(CBasePlayer *pPlayer);
	CNPC_Antlion* Bug_Create(const Vector &position, const QAngle &angles, const Vector &velocity, const QAngle &angVelocity, CBaseEntity *owner);

	bool	HasAnyAmmo(void) { return true; }

	bool	Reload(void);

	bool	ShouldDisplayHUDHint() { return true; }

	DECLARE_DATADESC();

protected:

	bool		m_bDrawBackFinished;
	bool		m_bRedraw;
	bool	    m_bWorker;
};

IMPLEMENT_SERVERCLASS_ST(CWeaponBugSpawner, DT_WeaponBugSpawner)
END_SEND_TABLE()

LINK_ENTITY_TO_CLASS(weapon_bugspawner, CWeaponBugSpawner);
#ifndef HL2MP
PRECACHE_WEAPON_REGISTER(weapon_bugspawner);
#endif

BEGIN_DATADESC(CWeaponBugSpawner)

DEFINE_FIELD(m_bRedraw, FIELD_BOOLEAN),
DEFINE_FIELD(m_bDrawBackFinished, FIELD_BOOLEAN),

END_DATADESC()

#define DEFAULT_HEALTH 50

extern ConVar bugspawner_hp("bugspawner_health", "50");
extern ConVar bugspawner_max_hp("bugspawner_max_health", "1500");

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : &position - 
//			&angles - 
//			&velocity - 
//			&angVelocity - 
//			*owner - 
// Output : CBaseGrenade
//-----------------------------------------------------------------------------
CNPC_Antlion* CWeaponBugSpawner::Bug_Create(const Vector &position, const QAngle &angles, const Vector &velocity, const QAngle &angVelocity, CBaseEntity *owner)
{
	CNPC_Antlion *pBuggo = (CNPC_Antlion *)CAI_BaseAntlionBase::Create("npc_antlion", position, angles); //,owner

	if (pBuggo != NULL)
	{
		pBuggo->SetLocalAngularVelocity(angVelocity);
		pBuggo->SetAbsVelocity(velocity);
		pBuggo->SetFollowTarget(owner);
		pBuggo->AddEntityRelationship(owner, D_LI, 99);
		pBuggo->AddSpawnFlags(133636);

		if (m_bWorker)
		{
			pBuggo->AddSpawnFlags(SF_ANTLION_WORKER);
		}
	}

	return pBuggo;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
CWeaponBugSpawner::CWeaponBugSpawner(void)
{
	m_bDrawBackFinished = false;
	m_bRedraw = false;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWeaponBugSpawner::Spawn(void)
{
	BaseClass::Spawn();

	// Increase the bugbait's pickup volume. It spawns inside the antlion guard's body,
	// and playtesters seem to be wary about moving into the body.
	SetSize(Vector(-4, -4, -4), Vector(4, 4, 4));
	CollisionProp()->UseTriggerBounds(true, 100);
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWeaponBugSpawner::FallInit(void)
{
	SetModel(GetWorldModel());

	VPhysicsDestroyObject();
	SetMoveType(MOVETYPE_FLYGRAVITY);
	SetSolid(SOLID_BBOX);
	AddSolidFlags(FSOLID_TRIGGER);

	SetPickupTouch();

	SetThink(&CBaseCombatWeapon::FallThink);

	SetNextThink(gpGlobals->curtime + 0.1f);
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWeaponBugSpawner::Precache(void)
{
	BaseClass::Precache();

	PrecacheScriptSound("Weapon_Bugbait.Splat");
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWeaponBugSpawner::Drop(const Vector &vecVelocity)
{
	BaseClass::Drop(vecVelocity);

	// On touch, stick & stop moving. Increase our thinktime a bit so we don't stomp the touch for a bit
	SetNextThink(gpGlobals->curtime + 3.0);
	SetTouch(&CWeaponBugSpawner::BugbaitStickyTouch);
}

//-----------------------------------------------------------------------------
// Purpose: Stick to the world when we touch it
//-----------------------------------------------------------------------------
void CWeaponBugSpawner::BugbaitStickyTouch(CBaseEntity *pOther)
{
	if (!pOther->IsWorld())
		return;

	// Stop moving, wait for pickup
	SetMoveType(MOVETYPE_NONE);
	SetThink(NULL);
	SetPickupTouch();
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pPicker - 
//-----------------------------------------------------------------------------
void CWeaponBugSpawner::OnPickedUp(CBaseCombatCharacter *pNewOwner)
{
	BaseClass::OnPickedUp(pNewOwner);
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWeaponBugSpawner::PrimaryAttack(void)
{
	if (m_bRedraw)
		return;

	CBaseCombatCharacter *pOwner = GetOwner();

	if (pOwner == NULL)
		return;

	CBasePlayer *pPlayer = ToBasePlayer(GetOwner());

	if (pPlayer == NULL)
		return;

	m_bWorker = false;

	SendWeaponAnim(ACT_VM_HAULBACK);

	m_flTimeWeaponIdle = FLT_MAX;
	m_flNextPrimaryAttack = FLT_MAX;

	m_iPrimaryAttacks++;
	gamestats->Event_WeaponFired(pPlayer, true, GetClassname());
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWeaponBugSpawner::SecondaryAttack(void)
{
	// Squeeze!
	if (m_bRedraw)
		return;

	CBaseCombatCharacter *pOwner = GetOwner();

	if (pOwner == NULL)
		return;

	CBasePlayer *pPlayer = ToBasePlayer(GetOwner());

	if (pPlayer == NULL)
		return;

	m_bWorker = true;

	SendWeaponAnim(ACT_VM_HAULBACK);

	m_flTimeWeaponIdle = FLT_MAX;
	m_flNextPrimaryAttack = FLT_MAX;

	m_iSecondaryAttacks++;
	gamestats->Event_WeaponFired(pPlayer, true, GetClassname());
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pPlayer - 
//-----------------------------------------------------------------------------
void CWeaponBugSpawner::ThrowGrenade(CBasePlayer *pPlayer)
{
	Vector	vForward, vRight, vUp, vThrowPos, vThrowVel;

	pPlayer->EyeVectors(&vForward, &vRight, &vUp);

	vThrowPos = pPlayer->Weapon_ShootPosition();

	vThrowPos += vForward * 18.0f;
	vThrowPos += vRight * 12.0f;

	pPlayer->GetVelocity(&vThrowVel, NULL);
	vThrowVel += vForward * 500;

	trace_t	tr;
	UTIL_TraceLine(pPlayer->EyePosition(), pPlayer->EyePosition() + (vForward * MAX_TRACE_LENGTH), MASK_NPCSOLID, pPlayer, COLLISION_GROUP_NONE, &tr);

	CNPC_Antlion* pAntlion = Bug_Create(tr.endpos, vec3_angle, vThrowVel, vec3_angle, pPlayer);

	if (pAntlion != NULL) {
		if (tr.fraction != 1.0) {
			DispatchSpawn(pAntlion);
			if (pAntlion->CapabilitiesGet() & bits_CAP_MOVE_FLY)
			{
				Vector pos = tr.endpos - vForward * 36;
				pAntlion->Teleport(&pos, NULL, NULL);
			}
			else
			{
				// raise z pos little up, drop antlion to floor
				tr.endpos.z += 12;
				pAntlion->Teleport(&tr.endpos, NULL, NULL);
				UTIL_DropToFloor(pAntlion, MASK_NPCSOLID);
			}

			Vector vUP = pAntlion->GetAbsOrigin();
			vUP.z += 1;
			AI_TraceHull(pAntlion->GetAbsOrigin(), vUP, pAntlion->GetHullMins(), pAntlion->GetHullMaxs(),
				MASK_NPCSOLID, pAntlion, COLLISION_GROUP_NONE, &tr);

			if ((tr.fraction < 1.0) || tr.startsolid)
			{
				pAntlion->SUB_Remove();
				DevMsg("Bad Position!\n");
			}
		}
		pAntlion->Activate();
		pAntlion->SetMaxHealth(bugspawner_max_hp.GetInt());
		if (bugspawner_hp.GetInt() <= bugspawner_max_hp.GetInt())
		{
			pAntlion->SetHealth(bugspawner_hp.GetInt());
		}
	}

	m_bRedraw = true;
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pEvent - 
//			*pOperator - 
//-----------------------------------------------------------------------------
void CWeaponBugSpawner::Operator_HandleAnimEvent(animevent_t *pEvent, CBaseCombatCharacter *pOperator)
{
	CBasePlayer *pOwner = ToBasePlayer(GetOwner());
	switch (pEvent->event)
	{
	case EVENT_WEAPON_SEQUENCE_FINISHED:
		m_bDrawBackFinished = true;
		break;

	case EVENT_WEAPON_THROW:
		ThrowGrenade(pOwner);
		break;

	default:
		BaseClass::Operator_HandleAnimEvent(pEvent, pOperator);
		break;
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CWeaponBugSpawner::Reload(void)
{
	if ((m_bRedraw) && (m_flNextPrimaryAttack <= gpGlobals->curtime))
	{
		//Redraw the weapon
		SendWeaponAnim(ACT_VM_DRAW);

		//Update our times
		m_flNextPrimaryAttack = gpGlobals->curtime + SequenceDuration();

		//Mark this as done
		m_bRedraw = false;
	}

	return true;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWeaponBugSpawner::ItemPostFrame(void)
{
	CBasePlayer *pOwner = ToBasePlayer(GetOwner());

	if (pOwner == NULL)
		return;

	// See if we're cocked and ready to throw
	if (m_bDrawBackFinished)
	{
		if ((pOwner->m_nButtons & IN_ATTACK) == false)
		{
			SendWeaponAnim(ACT_VM_THROW);
			m_flNextPrimaryAttack = gpGlobals->curtime + SequenceDuration();
			m_bDrawBackFinished = false;
		}
	}
	else
	{
		//See if we're attacking
		if ((pOwner->m_nButtons & IN_ATTACK) && (m_flNextPrimaryAttack < gpGlobals->curtime))
		{
			PrimaryAttack();
		}
		else if ((pOwner->m_nButtons & IN_ATTACK2) && (m_flNextSecondaryAttack < gpGlobals->curtime))
		{
			SecondaryAttack();
		}
	}

	if (m_bRedraw)
	{
		if (IsViewModelSequenceFinished())
		{
			Reload();
		}
	}

	WeaponIdle();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
bool CWeaponBugSpawner::Deploy(void)
{
	CBasePlayer *pOwner = ToBasePlayer(GetOwner());

	if (pOwner == NULL)
		return false;

	m_bRedraw = false;
	m_bDrawBackFinished = false;

	return BaseClass::Deploy();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
bool CWeaponBugSpawner::Holster(CBaseCombatWeapon *pSwitchingTo)
{
	m_bRedraw = false;
	m_bDrawBackFinished = false;

	return BaseClass::Holster(pSwitchingTo);
}
