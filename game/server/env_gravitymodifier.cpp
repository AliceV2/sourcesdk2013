// 
// Purpose: i love dicks
// 

#include "cbase.h"
#include "props.h"
#include "physics.h"
#include "physics_shared.h"
#include "movevars_shared.h"

class CEnvGravityMod : public CLogicalEntity
{
public:
	DECLARE_CLASS(CEnvGravityMod, CLogicalEntity);
	DECLARE_DATADESC();


	CEnvGravityMod()
	{
		m_currentGravity.Init();
	}


	void InputTurnOn(inputdata_t &inputData);
	void InputTurnOff(inputdata_t &inputData);
	void ResetPhysObjects();

private:
	Vector	m_currentGravity;
};

LINK_ENTITY_TO_CLASS(env_gravitymodifier, CEnvGravityMod);


BEGIN_DATADESC(CEnvGravityMod)

DEFINE_FIELD(m_currentGravity, FIELD_VECTOR),


DEFINE_KEYFIELD(m_currentGravity, FIELD_VECTOR, "currentgravity"),


DEFINE_INPUTFUNC(FIELD_VOID, "TurnOn", InputTurnOn),
DEFINE_INPUTFUNC(FIELD_VOID, "TurnOff", InputTurnOff),

END_DATADESC()

void CEnvGravityMod::InputTurnOn(inputdata_t &inputData)
{
	physenv->SetGravity(m_currentGravity);
	ResetPhysObjects();
}

void CEnvGravityMod::InputTurnOff(inputdata_t &inputData)
{
	physenv->SetGravity(Vector(0, 0, -600));
	ResetPhysObjects();
}

void CEnvGravityMod::ResetPhysObjects(void)
{
	CBaseEntity* pResult = gEntList.FindEntityByClassname(NULL, "prop_physics");
	while (pResult)
	{
		CPhysicsProp* pPrp = dynamic_cast<CPhysicsProp*>(pResult);
		if (pPrp)
			pPrp->VPhysicsGetObject()->Wake();

		pResult = gEntList.FindEntityByClassname(pResult, "prop_physics");
	}
}
